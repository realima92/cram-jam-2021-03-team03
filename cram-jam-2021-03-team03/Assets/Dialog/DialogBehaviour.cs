﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBehaviour : MonoBehaviour
{
    public static DialogBehaviour instance;

    [SerializeField] DialogAnimationController _visual;
    [SerializeField] InteractionBehavior _button;

    [SerializeField] Text nameUI, textUI;
    [SerializeField] Image actorImage;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        var intro = LevelManager.instance.introDialog;
        if (intro)
        {
            NextDialog(intro);
        }
    }

    public void NextDialog(Dialog dialog)
    {
        nameUI.text = dialog.SpeakerName;
        textUI.text = dialog.text;
        actorImage.sprite = dialog.sprite;

        if (dialog.options?.Count > 0)
            SetOptions(dialog.options);

        _visual.ShowDialog();
        _button.gameObject.SetActive(true);
    }

    internal void EndDialog()
    {
        _visual.HideDialog();
        _button.gameObject.SetActive(false);
    }

    public void SetOptions(List<DialogOptions> options)
    {
        _button.options = options;
    }
}
