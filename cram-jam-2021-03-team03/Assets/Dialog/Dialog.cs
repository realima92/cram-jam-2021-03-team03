﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Dialog@", menuName = "Game/Dialog")]
public class Dialog : ScriptableObject
{
    public enum Speaker { NONE, JOAO_WATS, O_CARA_DO_CACHORRO, VO_DO_BIRA, PAOLO, JUDAS, DETETIVE_ESTRANHO };
    [SerializeField] private Speaker speaker;
    public string SpeakerName { get
        {
            switch (speaker)
            {
                case Speaker.DETETIVE_ESTRANHO: return "Detetive Estranho";
                case Speaker.JOAO_WATS: return "Joao Wats"; 
                case Speaker.JUDAS: return "Judas"; 
                case Speaker.PAOLO: return "Paolo";
                case Speaker.O_CARA_DO_CACHORRO: return "O Cara do Cachorro";
                case Speaker.VO_DO_BIRA: return "Vo do Bira";
                default: return "???";
            }
        } 
    }

    [Multiline]
    public string text;
    public Sprite sprite;
    public List<DialogOptions> options = new List<DialogOptions>() { new DialogOptions() };

}

[System.Serializable]
public class DialogOptions
{
    public string name;
    public Dialog triggeredDialog;
    public UnityEvent triggeredEvents;

    public DialogOptions()
    {
        name = "Continue";
    }
}
