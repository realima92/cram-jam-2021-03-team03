﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class DialogAnimationController : MonoBehaviour
{
    public enum TransitionType { Fade, Tween }
    public TransitionType transition;

    [SerializeField] Image actorImage;
    [SerializeField] Text textUI;
    
    private Animator _anim;

    // Start is called before the first frame update
    void Start()
    {
        _anim = GetComponent<Animator>();
        HideDialog(skipTransition: true);
    }

    public void HideDialog(bool skipTransition = false)
    {
        _anim.SetBool("TransitionState", false);
        if (skipTransition)
            _anim.Play(_anim.GetCurrentAnimatorStateInfo(-1).fullPathHash, -1, 1);
    }

    public void ShowDialog()
    {
        _anim.SetBool("TransitionState", true);
    }
}
