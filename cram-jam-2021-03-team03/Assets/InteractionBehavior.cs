﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionBehavior : MonoBehaviour
{
    public List<DialogOptions> options;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayInteraction(int index)
    {
        if (options[index] != null)
        {
            if (options[index].triggeredDialog)
            {
                DialogBehaviour.instance.NextDialog(options[index].triggeredDialog);
            }
            else
            {
                DialogBehaviour.instance.EndDialog();
            }
        }
    }
}
