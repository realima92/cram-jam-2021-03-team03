﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagColisor : MonoBehaviour
{
    [SerializeField] private GameObject interactionPanel;
    private InteractionBehavior interaction;
    private bool interactionIsShown;

    public InteractionBehavior Interaction => interaction;

    // Start is called before the first frame update
    void Start()
    {
        interactionPanel?.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Interactions"))
        {
            interaction = collision.gameObject.GetComponent<InteractionBehavior>();
            Invoke("ShowInteractions", 1);
        }


    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Interactions") && collision.gameObject.GetComponent<InteractionBehavior>() == interaction)
        {
            if (interactionIsShown)
            {
                //Debug.Log("HideInteractions");
                if (interactionPanel)
                {
                    interactionPanel.SetActive(false);
                }
                interactionIsShown = false;
            }
            interaction = null;
            CancelInvoke("ShowInteractions");
        }
    }

    public void ShowInteractions()
    {
        //Debug.Log("Show interactions");
        if (interactionPanel)
        {
            interactionPanel.transform.position = transform.position;
            interactionPanel.SetActive(true);
        }

        interactionIsShown = true;
    }




}
