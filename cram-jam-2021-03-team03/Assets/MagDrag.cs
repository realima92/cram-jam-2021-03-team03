﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class MagDrag : MonoBehaviour
{
    public   GameObject mag;
    private bool moving;
    private float startPosX;
    private float startPosY;
    Vector2 startMagPosition;

    public MagColisor colisor;

 
    void Update()
    {
        if (moving)
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);        
            mag.transform.position = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, mag.transform.localPosition.z);
            

        }
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            startPosX = mousePos.x - mag.transform.localPosition.x;
            startPosY = mousePos.y - mag.transform.localPosition.y;
            moving = true;
            
           
        }
    }


    private void OnMouseUp()
    {
        moving = false;         

        if(colisor.Interaction && colisor.Interaction.options.Count > 0)
        {
            colisor.Interaction.PlayInteraction(0);
        }
    }

}
